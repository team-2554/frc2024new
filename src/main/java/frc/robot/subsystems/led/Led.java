package frc.robot.subsystems.led;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.littletonrobotics.junction.Logger;

public class Led extends SubsystemBase {

  AddressableLED m_led = new AddressableLED(0);
  AddressableLEDBuffer m_ledBuffer = new AddressableLEDBuffer(350);
  int m_rainbowFirstPixelHue = 0;
  String colorVal;

  public Led() {
    m_led.setLength(m_ledBuffer.getLength());
    for (var i = 0; i < m_ledBuffer.getLength(); i++) {
      m_rainbowFirstPixelHue %= 180;
      // Calculate the hue - hue is easier for rainbows because the color
      // shape is a circle so only one value needs to precess
      final var hue = (m_rainbowFirstPixelHue + (i * 180 / m_ledBuffer.getLength())) % 180;
      // Set the value
      m_ledBuffer.setHSV(i, hue, 255, 255);
      // Increase by to make the rainbow "move"
      // Check bounds
    }
    m_led.setData(m_ledBuffer);
    m_led.start();
  }

  public void periodic() {
    Logger.recordOutput("Led/ColorValue", colorVal);
  }

  public void rainbow() {
    // For every pixel
    m_rainbowFirstPixelHue += 3;
    for (var i = 0; i < m_ledBuffer.getLength(); i++) {
      m_rainbowFirstPixelHue %= 180;
      // Calculate the hue - hue is easier for rainbows because the color
      // shape is a circle so only one value needs to precess
      final var hue = (m_rainbowFirstPixelHue + ((i * 8) * 180 / m_ledBuffer.getLength())) % 180;
      // Set the value
      m_ledBuffer.setHSV(i, hue, 255, 255);
      // Increase by to make the rainbow "move"
      // Check bounds
    }
    m_led.setData(m_ledBuffer);
    colorVal = "rainbow";
  }

  public void greenGold() {
    for (var i = 0; i < m_ledBuffer.getLength(); i++) {
      if (i % 2 == 0) {
        m_ledBuffer.setHSV(i, 42, 255, 255);
      } else if (i % 3 == 0) {
        m_ledBuffer.setHSV(i, 93, 255, 255);
      }
    }
    m_led.setData(m_ledBuffer);
    colorVal = "greengold";
  }

  int countSpacer = 0;

  public void shoot() {
    if (countSpacer % 15 == 0) {
      for (var i = 0; i < m_ledBuffer.getLength(); i++) {
        // Sets the specified LED to the RGB values for red
        m_ledBuffer.setHSV(i, 0, 0, 0);
      }

    } else {
      for (var i = 0; i < m_ledBuffer.getLength(); i++) {
        // Sets the specified LED to the RGB values for red
        m_ledBuffer.setHSV(i, 0, 255, 255);
      }
    }

    countSpacer++;
    colorVal = "red";

    m_led.setData(m_ledBuffer);
  }

  public void distance(double distance, boolean isNote) {
    Logger.recordOutput("Distance/speaker", distance);
    if (distance < 0.1) {
      for (var i = 0; i < m_ledBuffer.getLength(); i++) {
        // Sets the specified LED to the RGB values for green
        m_ledBuffer.setHSV(i, 120, 255, 255);
      }
      colorVal = "green";

    } else if (distance < 0.5) {
      for (var i = 0; i < m_ledBuffer.getLength(); i++) {
        // Sets the specified LED to the RGB values for yellow
        m_ledBuffer.setHSV(i, 60, 255, 255);
      }
      colorVal = "yellow";

    } else if (distance >= 0.5 && !isNote) {
      for (var i = 0; i < m_ledBuffer.getLength(); i++) {
        m_ledBuffer.setHSV(i, 41, 255, 255);
      }
      colorVal = "orange";
    } else if (distance >= 0.5 && isNote) {
      greenGold();
    }
    m_led.setData(m_ledBuffer);
  }
}
