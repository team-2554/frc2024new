package frc.robot.subsystems.climber;

import org.littletonrobotics.junction.AutoLog;

public interface ClimberIO {
  @AutoLog
  public static class ClimberIOInputs {
    public double speed = 0.0;
  }

  public default void updateInputs(ClimberIOInputs inputs) {}

  public default void setSpeed(double speed) {}

  public default void stop() {}

  public default void setWithPID(double setpoint) {}
}
