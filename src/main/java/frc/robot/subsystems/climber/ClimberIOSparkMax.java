package frc.robot.subsystems.climber;

import com.revrobotics.CANSparkBase.ControlType;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.SparkPIDController;
import frc.robot.Constants;
import org.littletonrobotics.junction.Logger;

public class ClimberIOSparkMax implements ClimberIO {

  private CANSparkMax climber;
  private SparkPIDController climbPID;

  public ClimberIOSparkMax() {
    climber = new CANSparkMax(Constants.climber, MotorType.kBrushless);
    climbPID = climber.getPIDController();
  }

  @Override
  public void updateInputs(ClimberIOInputs inputs) {
    inputs.speed = climber.getEncoder().getVelocity();
    Logger.recordOutput("Climb/pos", climber.getEncoder().getPosition());
  }

  @Override
  public void setSpeed(double speed) {
    climber.set(speed);
  }

  @Override
  public void stop() {
    climber.stopMotor();
  }

  @Override
  public void setWithPID(double setpoint) {
    climbPID.setReference(setpoint, ControlType.kSmartMotion);
  }
}
