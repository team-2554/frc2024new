package frc.robot.subsystems.climber;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.littletonrobotics.junction.Logger;

public class Climber extends SubsystemBase {

  private final ClimberIO mClimber;
  private final ClimberIOInputsAutoLogged inputs = new ClimberIOInputsAutoLogged();

  public Climber(ClimberIO climber) {
    mClimber = climber;
  }

  public void periodic() {
    mClimber.updateInputs(inputs);
    Logger.processInputs("Climber/Climber", inputs);
  }

  public double getSpeed() {
    return inputs.speed;
  }

  public void stop() {
    mClimber.stop();
  }

  public void setSpeed(double speed) {
    mClimber.setSpeed(speed);
  }

  public void setWithPID(double setpoint) {
    mClimber.setWithPID(setpoint);
  }
}
