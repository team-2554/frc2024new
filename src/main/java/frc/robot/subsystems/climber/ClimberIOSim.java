package frc.robot.subsystems.climber;

import edu.wpi.first.wpilibj.simulation.PWMSim;

public class ClimberIOSim implements ClimberIO {

  private PWMSim climber;

  public ClimberIOSim() {
    climber = new PWMSim(200);
  }

  @Override
  public void updateInputs(ClimberIOInputs inputs) {
    inputs.speed = climber.getSpeed();
  }

  @Override
  public void setSpeed(double speed) {
    climber.setSpeed(speed);
  }

  @Override
  public void stop() {
    climber.setSpeed(0);
  }
}
