package frc.robot.subsystems.vision;

import org.littletonrobotics.junction.AutoLog;

public interface LimelightIO {
  @AutoLog
  public static class LimelightIOInputs {
    public double x = 0.0;
    public double y = 0.0;
    public double area = 0.0;
    public double hasObjects = 0.0;
  }

  public default void setPipeline(int pipeline) {}

  public default void getXOffset() {}

  public default void getYOffset() {}

  public default void getAreaOffset() {}

  public default void hasTargets() {}
}
