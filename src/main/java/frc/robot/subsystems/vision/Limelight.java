package frc.robot.subsystems.vision;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.littletonrobotics.junction.Logger;

public class Limelight extends SubsystemBase {

  private final LimelightIOInputsAutoLogged inputs = new LimelightIOInputsAutoLogged();
  private final NetworkTable table;

  public Limelight() {
    table = NetworkTableInstance.getDefault().getTable("limelight");
  }

  public void periodic() {
    inputs.x = table.getEntry("tx").getDouble(0.0);
    inputs.y = table.getEntry("ty").getDouble(0.0);
    inputs.area = table.getEntry("ta").getDouble(0.0);
    inputs.hasObjects = table.getEntry("tv").getDouble(0.0);

    Logger.recordOutput("Limelight/X", inputs.x);
    Logger.recordOutput("Limelight/Y", inputs.y);
    Logger.recordOutput("Limelight/Area", inputs.area);
  }

  public void setPipeline(int pipeline) {
    table.getEntry("Pipeline").setNumber(pipeline);
  }

  public double getXOffset() {
    return inputs.x;
  }

  public double getYOffset() {
    return inputs.y;
  }

  public double getAreaOffset() {
    return inputs.area;
  }

  public boolean hasObjects() {
    if (inputs.hasObjects == 0) {
      return false;
    } else {
      return true;
    }
  }
}
