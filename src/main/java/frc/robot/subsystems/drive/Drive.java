// Copyright 2021-2024 FRC 6328
// http://github.com/Mechanical-Advantage
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// version 3 as published by the Free Software Foundation or
// available in the root directory of this project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

package frc.robot.subsystems.drive;

import static edu.wpi.first.units.Units.*;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.path.PathConstraints;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PathPlannerLogging;
import com.pathplanner.lib.util.ReplanningConfig;
import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Twist2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine;
import frc.robot.commands.DriveCommands;
import frc.robot.util.PhotonVision;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.DoubleSupplier;
import java.util.function.Supplier;
import org.littletonrobotics.junction.AutoLogOutput;
import org.littletonrobotics.junction.Logger;

public class Drive extends SubsystemBase {

  // Drive constants
  private static final double MAX_LINEAR_SPEED = Units.feetToMeters(10);
  private static final double TRACK_WIDTH_X = Units.inchesToMeters(23.5);
  private static final double TRACK_WIDTH_Y = Units.inchesToMeters(23.1);
  private static final double DRIVE_BASE_RADIUS =
      Math.hypot(TRACK_WIDTH_X / 2.0, TRACK_WIDTH_Y / 2.0);
  private static final double MAX_ANGULAR_SPEED = MAX_LINEAR_SPEED / DRIVE_BASE_RADIUS;

  // FIX POSES
  public final Pose2d sourceBlue = new Pose2d(15.14, 1.41, Rotation2d.fromDegrees(-57.08));
  public final Pose2d speakerBlue = new Pose2d(3.97, 5.47, Rotation2d.fromDegrees(178.13));
  public final Pose2d ampBlue = new Pose2d(1.8, 7.37, Rotation2d.fromDegrees(89.28));
  public final Pose2d speakerFieldBlue = new Pose2d(0, 5.625, new Rotation2d());

  public final Pose2d speakerFieldRed = new Pose2d(16, 5.54, new Rotation2d());
  public final Pose2d speakerRed = new Pose2d(13.91, 4.3, Rotation2d.fromDegrees(-149.35));
  public final Pose2d sourceRed = new Pose2d(1.25, 1.7, Rotation2d.fromDegrees(-26.57));
  public final Pose2d desiredShootPoseRed = new Pose2d(14.00, 4.34, Rotation2d.fromDegrees(-14.46));
  public final Pose2d desiredShootPoseBlue = new Pose2d(2.61, 4.82, Rotation2d.fromDegrees(125.39));

  public final Supplier<Pose2d> desiredPoseSupplier =
      () ->
          (DriverStation.getAlliance().orElse(Alliance.Red) == Alliance.Red)
              ? desiredShootPoseRed
              : desiredShootPoseBlue;
  public final Supplier<Pose2d> alliancePoseSpeakerSupplier = desiredPoseSupplier;
  public final Supplier<Pose2d> alliancePoseSpeakerFieldSupplier =
      () ->
          (DriverStation.getAlliance().orElse(Alliance.Red) == Alliance.Blue)
              ? speakerFieldBlue
              : speakerFieldRed;
  public final Supplier<Pose2d> allianceSourceSupplier =
      () ->
          (DriverStation.getAlliance().orElse(Alliance.Red) == Alliance.Blue)
              ? sourceBlue
              : sourceRed;

  private final PhotonVision pVision = new PhotonVision();

  private final PathConstraints constraints =
      new PathConstraints(3, 4, Units.degreesToRadians(540), Units.degreesToRadians(720));

  static final Lock odometryLock = new ReentrantLock();
  private final GyroIO gyroIO;
  private final GyroIOInputsAutoLogged gyroInputs = new GyroIOInputsAutoLogged();
  private final Module[] modules = new Module[4]; // FL, FR, BL, BR
  private final SysIdRoutine sysId;
  private double shootDistance = 0;
  private SwerveDriveKinematics kinematics = new SwerveDriveKinematics(getModuleTranslations());
  private Rotation2d rawGyroRotation = new Rotation2d();
  private SwerveModulePosition[] lastModulePositions = // For delta tracking
      new SwerveModulePosition[] {
        new SwerveModulePosition(),
        new SwerveModulePosition(),
        new SwerveModulePosition(),
        new SwerveModulePosition()
      };
  // Dummy pose for twist
  private Pose2d pose2 = new Pose2d();

  private SwerveDrivePoseEstimator poseEstimator =
      new SwerveDrivePoseEstimator(kinematics, rawGyroRotation, lastModulePositions, new Pose2d());

  public Drive(
      GyroIO gyroIO,
      ModuleIO flModuleIO,
      ModuleIO frModuleIO,
      ModuleIO blModuleIO,
      ModuleIO brModuleIO) {
    this.gyroIO = gyroIO;
    modules[0] = new Module(flModuleIO, 0);
    modules[1] = new Module(frModuleIO, 1);
    modules[2] = new Module(blModuleIO, 2);
    modules[3] = new Module(brModuleIO, 3);

    // Start threads (no-op for each if no signals have been created)
    SparkMaxOdometryThread.getInstance().start();

    // Configure AutoBuilder for PathPlanner
    AutoBuilder.configureHolonomic(
        this::getPose,
        this::setPose,
        () -> kinematics.toChassisSpeeds(getModuleStates()),
        this::runVelocity,
        new HolonomicPathFollowerConfig(
            MAX_LINEAR_SPEED, DRIVE_BASE_RADIUS, new ReplanningConfig()),
        () ->
            DriverStation.getAlliance().isPresent()
                && (DriverStation.getAlliance().get() == Alliance.Red),
        this);
    PathPlannerLogging.setLogActivePathCallback(
        (activePath) -> {
          Logger.recordOutput(
              "Odometry/Trajectory", activePath.toArray(new Pose2d[activePath.size()]));
        });
    PathPlannerLogging.setLogTargetPoseCallback(
        (targetPose) -> {
          Logger.recordOutput("Odometry/TrajectorySetpoint", targetPose);
        });
    Logger.recordOutput(
        "Poses/Speaker",
        (DriverStation.getAlliance().isPresent()
                && DriverStation.getAlliance().get() == Alliance.Red)
            ? speakerRed
            : speakerBlue);
    poseEstimator.setVisionMeasurementStdDevs(
        VecBuilder.fill(1.5, 1.5, Units.degreesToRadians(50)));
    // Configure SysId
    sysId =
        new SysIdRoutine(
            new SysIdRoutine.Config(
                null,
                null,
                null,
                (state) -> Logger.recordOutput("Drive/SysIdState", state.toString())),
            new SysIdRoutine.Mechanism(
                (voltage) -> {
                  for (int i = 0; i < 4; i++) {
                    modules[i].runCharacterization(voltage.in(Volts));
                  }
                },
                null,
                this));
    Logger.recordOutput("Poses/speakerFIeld", alliancePoseSpeakerFieldSupplier.get());
  }

  public void periodic() {
    if (DriverStation.getAlliance().isPresent()) {
      shootDistance =
          alliancePoseSpeakerFieldSupplier
              .get()
              .relativeTo(desiredPoseSupplier.get())
              .getTranslation()
              .getNorm();
    }
    odometryLock.lock(); // Prevents odometry updates while reading data
    gyroIO.updateInputs(gyroInputs);
    for (var module : modules) {
      module.updateInputs();
    }
    odometryLock.unlock();

    var poseEstimated = pVision.getEstimatedGlobalPose(getPose());

    if (poseEstimated.isPresent()) {
      poseEstimator.addVisionMeasurement(
          poseEstimated.get().estimatedPose.toPose2d(), poseEstimated.get().timestampSeconds);
    }

    // Limelight pose estimator
    // Don't change alliance pose estimated as in 2024 it is updated to use single
    // coordinate system!
    // LimelightHelpers.PoseEstimate limelightEstimate =
    // LimelightHelpers.getBotPoseEstimate_wpiBlue("limelight");

    // if (limelightEstimate.tagCount >= FREQUENT) {
    //   poseEstimator.setVisionMeasurementStdDevs(VecBuilder.fill(0.7, 0.7,
    // Units.degreesToRadians(30)));
    //   poseEstimator.addVisionMeasurement(limelightEstimate.pose,
    // limelightEstimate.timestampSeconds);
    //   Logger.recordOutput("Drive/limelightPose", limelightEstimate.pose);
    // }

    Logger.processInputs("Drive/Gyro", gyroInputs);
    for (var module : modules) {
      module.periodic();
    }

    // Stop moving when disabled
    if (DriverStation.isDisabled()) {
      for (var module : modules) {
        module.stop();
      }
    }
    // Log empty setpoint states when disabled
    if (DriverStation.isDisabled()) {
      Logger.recordOutput("SwerveStates/Setpoints", new SwerveModuleState[] {});
      Logger.recordOutput("SwerveStates/SetpointsOptimized", new SwerveModuleState[] {});
    }

    // Update odometry
    double[] sampleTimestamps =
        modules[0].getOdometryTimestamps(); // All signals are sampled together
    int sampleCount = sampleTimestamps.length;
    for (int i = 0; i < sampleCount; i++) {
      // Read wheel positions and deltas from each module
      SwerveModulePosition[] modulePositions = new SwerveModulePosition[4];
      SwerveModulePosition[] moduleDeltas = new SwerveModulePosition[4];
      for (int moduleIndex = 0; moduleIndex < 4; moduleIndex++) {
        modulePositions[moduleIndex] = modules[moduleIndex].getOdometryPositions()[i];
        moduleDeltas[moduleIndex] =
            new SwerveModulePosition(
                modulePositions[moduleIndex].distanceMeters
                    - lastModulePositions[moduleIndex].distanceMeters,
                modulePositions[moduleIndex].angle);
        lastModulePositions[moduleIndex] = modulePositions[moduleIndex];
      }

      // Update gyro angle
      if (gyroInputs.connected) {
        // Use the real gyro angle
        rawGyroRotation = gyroInputs.odometryYawPositions[i];
      } else {
        // Use the angle delta from the kinematics and module deltas
        Twist2d twist = kinematics.toTwist2d(moduleDeltas);
        rawGyroRotation = rawGyroRotation.plus(new Rotation2d(twist.dtheta));
      }
      Twist2d twist = kinematics.toTwist2d(moduleDeltas);
      pose2 = pose2.exp(twist);

      Logger.recordOutput("fromTwist", pose2);
      // Apply update
      poseEstimator.updateWithTime(sampleTimestamps[i], (rawGyroRotation), modulePositions);
    }
  }

  /**
   * Runs the drive at the desired velocity.
   *
   * @param speeds Speeds in meters/sec
   */
  public void runVelocity(ChassisSpeeds speeds) {
    // Calculate module setpoints
    ChassisSpeeds discreteSpeeds = ChassisSpeeds.discretize(speeds, 0.02);
    SwerveModuleState[] setpointStates = kinematics.toSwerveModuleStates(discreteSpeeds);
    SwerveDriveKinematics.desaturateWheelSpeeds(setpointStates, MAX_LINEAR_SPEED);

    // Send setpoints to modules
    SwerveModuleState[] optimizedSetpointStates = new SwerveModuleState[4];
    for (int i = 0; i < 4; i++) {
      // The module returns the optimized state, useful for logging
      optimizedSetpointStates[i] = modules[i].runSetpoint(setpointStates[i]);
    }

    // Log setpoint states
    Logger.recordOutput("SwerveStates/Setpoints", setpointStates);
    Logger.recordOutput("SwerveStates/SetpointsOptimized", optimizedSetpointStates);
  }

  /** Stops the drive. */
  public void stop() {
    runVelocity(new ChassisSpeeds());
  }

  /**
   * Stops the drive and turns the modules to an X arrangement to resist movement. The modules will
   * return to their normal orientations the next time a nonzero velocity is requested.
   */
  public void stopWithX() {
    Rotation2d[] headings = new Rotation2d[4];
    for (int i = 0; i < 4; i++) {
      headings[i] = getModuleTranslations()[i].getAngle();
    }
    kinematics.resetHeadings(headings);
    stop();
  }

  /** Returns a command to run a quasistatic test in the specified direction. */
  public Command sysIdQuasistatic(SysIdRoutine.Direction direction) {
    return sysId.quasistatic(direction);
  }

  /** Returns a command to run a dynamic test in the specified direction. */
  public Command sysIdDynamic(SysIdRoutine.Direction direction) {
    return sysId.dynamic(direction);
  }

  /** Returns the module states (turn angles and drive velocities) for all of the modules. */
  @AutoLogOutput(key = "SwerveStates/Measured")
  private SwerveModuleState[] getModuleStates() {
    SwerveModuleState[] states = new SwerveModuleState[4];
    for (int i = 0; i < 4; i++) {
      states[i] = modules[i].getState();
    }
    return states;
  }

  /** Returns the module positions (turn angles and drive positions) for all of the modules. */
  private SwerveModulePosition[] getModulePositions() {
    SwerveModulePosition[] states = new SwerveModulePosition[4];
    for (int i = 0; i < 4; i++) {
      states[i] = modules[i].getPosition();
    }
    return states;
  }

  /** Returns the current odometry pose. */
  @AutoLogOutput(key = "Odometry/Robot")
  public Pose2d getPose() {
    return poseEstimator.getEstimatedPosition();
  }

  /** Returns the current odometry rotation. */
  public Rotation2d getRotation() {
    return getPose().getRotation();
  }

  /** Resets the current odometry pose. */
  public void setPose(Pose2d pose) {
    poseEstimator.resetPosition(rawGyroRotation, getModulePositions(), pose);
  }

  /**
   * Adds a vision measurement to the pose estimator.
   *
   * @param visionPose The pose of the robot as measured by the vision camera.
   * @param timestamp The timestamp of the vision measurement in seconds.
   */
  public void addVisionMeasurement(Pose2d visionPose, double timestamp) {
    poseEstimator.addVisionMeasurement(visionPose, timestamp);
  }

  /** Returns the maximum linear speed in meters per sec. */
  public double getMaxLinearSpeedMetersPerSec() {
    return MAX_LINEAR_SPEED;
  }

  /** Returns the maximum angular speed in radians per sec. */
  public double getMaxAngularSpeedRadPerSec() {
    return MAX_ANGULAR_SPEED;
  }

  /** Returns an array of module translations. */
  public static Translation2d[] getModuleTranslations() {
    return new Translation2d[] {
      new Translation2d(TRACK_WIDTH_X / 2.0, TRACK_WIDTH_Y / 2.0), // FL
      new Translation2d(TRACK_WIDTH_X / 2.0, -TRACK_WIDTH_Y / 2.0), // FR
      new Translation2d(-TRACK_WIDTH_X / 2.0, TRACK_WIDTH_Y / 2.0), // BL
      new Translation2d(-TRACK_WIDTH_X / 2.0, -TRACK_WIDTH_Y / 2.0) // BR
    };
  }

  public void resetGyro() {
    gyroIO.resetGyro();
  }

  public Command goToSource() {
    return AutoBuilder.pathfindToPose(allianceSourceSupplier.get(), constraints, 0.0, 0.0);
  }

  public Command goToSpeaker() {
    return AutoBuilder.pathfindToPose(alliancePoseSpeakerSupplier.get(), constraints, 1.0, 0.0);
  }

  public Command goToAmp() {
    return AutoBuilder.pathfindToPose(ampBlue, constraints, 0.0, 0.0);
  }

  public double poseRadius() {
    Pose2d pose = getPose();
    double r =
        Math.sqrt(
            Math.pow(alliancePoseSpeakerFieldSupplier.get().getX() - pose.getX(), 2)
                + Math.pow(alliancePoseSpeakerFieldSupplier.get().getY() - pose.getY(), 2));

    return Math.abs(r - shootDistance);
  }

  public DoubleSupplier getAngleToPose(Pose2d target) {
    return () -> {
      Pose2d pose = getPose();

      double dx = target.getX() - pose.getX();
      double dy = target.getY() - pose.getY();
      double angle = (Math.atan(dy / dx));
      double radians = pose.getRotation().getRadians();
      // radians += Math.PI * 2;
      // radians %= (Math.PI * 2);
      // radians -= Math.PI;

      return radians - angle;
    };
  }

  public Command turnToSpeaker() {
    return DriveCommands.turnToAnglePID(
        getAngleToPose(alliancePoseSpeakerFieldSupplier.get()), this);
  }

  public Command turnToSpeaker(DoubleSupplier xSupplier, DoubleSupplier ySupplier) {
    return DriveCommands.turnToAnglePID(
        getAngleToPose(alliancePoseSpeakerFieldSupplier.get()), this, xSupplier, ySupplier);
  }
}
