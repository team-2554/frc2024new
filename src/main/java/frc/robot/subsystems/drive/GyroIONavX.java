// Copyright 2021-2023 FRC 6328
// http://github.com/Mechanical-Advantage
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// version 3 as published by the Free Software Foundation or
// available in the root directory of this project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

package frc.robot.subsystems.drive;

import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.SPI;
import java.util.OptionalDouble;
import java.util.Queue;
import org.littletonrobotics.junction.Logger;

/** IO implementation for NavX */
public class GyroIONavX implements GyroIO {
  byte updateRate = 50;
  private final AHRS navx = new AHRS(SPI.Port.kMXP);
  private final Queue<Double> yawPositionQueue;
  private final Queue<Double> yawTimestampQueue;

  public GyroIONavX() {
    if (!navx.isCalibrating()) {
      navx.reset();
    } else {
      // Wait for calibration to finish, then reset
      new Thread(
              () -> {
                try {
                  // 1 second calibration time after factory calibration
                  Thread.sleep((1000));
                  navx.reset();
                } catch (Exception e) {
                }
              })
          .start();
    }
    yawTimestampQueue = SparkMaxOdometryThread.getInstance().makeTimestampQueue();
    yawPositionQueue =
        SparkMaxOdometryThread.getInstance()
            .registerSignal(
                () -> {
                  boolean valid = navx.isConnected();
                  if (valid) {
                    return OptionalDouble.of(-navx.getFusedHeading());
                  } else {
                    return OptionalDouble.empty();
                  }
                });
  }

  @Override
  public void updateInputs(GyroIOInputs inputs) {
    inputs.connected = navx.isConnected();
    inputs.yawPosition = Rotation2d.fromDegrees(-navx.getFusedHeading());
    inputs.yawVelocityRadPerSec = Units.degreesToRadians(-navx.getRate());
    inputs.odometryYawTimestamps =
        yawTimestampQueue.stream().mapToDouble((Double value) -> value).toArray();
    inputs.odometryYawPositions =
        yawPositionQueue.stream()
            .map((Double value) -> Rotation2d.fromDegrees(value))
            .toArray(Rotation2d[]::new);
    yawTimestampQueue.clear();
    yawPositionQueue.clear();

    Logger.recordOutput("Gyro/fusedHeading", navx.getFusedHeading());
    Logger.recordOutput("Gyro/compassHeading", navx.getCompassHeading());
    Logger.recordOutput("Gyro/compassX", navx.getRawMagX());
    Logger.recordOutput("Gyro/compassY", navx.getRawMagY());
    Logger.recordOutput("Gyro/compassZ", navx.getRawMagZ());
    Logger.recordOutput("Gyro/accelX", navx.getRawAccelX());
    Logger.recordOutput("Gyro/accelY", navx.getRawAccelY());
    Logger.recordOutput("Gyro/accelZ", navx.getRawAccelZ());
  }

  @Override
  public void resetGyro() {
    navx.reset();
  }
}
