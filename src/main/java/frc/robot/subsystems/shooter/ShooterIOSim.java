package frc.robot.subsystems.shooter;

import edu.wpi.first.wpilibj.simulation.PWMSim;

public class ShooterIOSim implements ShooterIO {

  private PWMSim shooterTopSim = new PWMSim(1);
  private PWMSim shooterBottomSim = new PWMSim(2);

  @Override
  public void updateInputs(ShooterIOInputs inputs) {
    inputs.speedLeft = shooterTopSim.getSpeed();
    inputs.speedRight = shooterBottomSim.getSpeed();
  }

  @Override
  public void setSpeed(double speedTop, double speedBottom) {
    shooterTopSim.setSpeed(speedTop);
    shooterTopSim.setSpeed(speedBottom);
  }

  @Override
  public void stop() {
    shooterTopSim.setSpeed(0);
    shooterBottomSim.setSpeed(0);
  }
}
