package frc.robot.subsystems.shooter;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.revrobotics.CANSparkBase.ControlType;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkLowLevel.PeriodicFrame;
import com.revrobotics.CANSparkMax;
import com.revrobotics.SparkAbsoluteEncoder;
import com.revrobotics.SparkPIDController;
import edu.wpi.first.math.controller.PIDController;
import frc.robot.Constants;
import org.littletonrobotics.junction.Logger;
import org.littletonrobotics.junction.networktables.LoggedDashboardNumber;

public class ShooterIOSparkMax implements ShooterIO {

  private final CANSparkMax shooterTop;
  private final CANSparkMax shooterBottom;
  private SparkPIDController pidControllerT;
  private SparkPIDController pidControllerB;
  private final WPI_VictorSPX pivotActuator;
  private final PIDController pivotController;
  private final SparkAbsoluteEncoder pivotEncoder;
  private final LoggedDashboardNumber pivotPidP;
  private final LoggedDashboardNumber pivotPidI;

  public ShooterIOSparkMax() {
    shooterTop = new CANSparkMax(Constants.shooterTop, MotorType.kBrushless);
    shooterBottom = new CANSparkMax(Constants.shooterBottom, MotorType.kBrushless);
    pivotActuator = new WPI_VictorSPX(Constants.pivotActuator);
    pivotPidP = new LoggedDashboardNumber("Shooter/pivotPidP", 69);
    pivotPidI = new LoggedDashboardNumber("Shooter/pivotPidI", 1.7);
    pivotEncoder = shooterTop.getAbsoluteEncoder();
    pivotEncoder.setPositionConversionFactor(1);
    pivotController = new PIDController(pivotPidP.get(), pivotPidI.get(), 0);

    // Config Top Shooter
    shooterTop.setCANTimeout(250);
    shooterTop.setPeriodicFramePeriod(PeriodicFrame.kStatus5, 50);
    shooterTop.setPeriodicFramePeriod(PeriodicFrame.kStatus6, 50);
    pidControllerT = shooterTop.getPIDController();
    pidControllerT.setFF(0.0002300000051036477);
    pidControllerT.setP(0);
    pidControllerT.setI(0);
    pidControllerT.setD(0);
    shooterTop.setSmartCurrentLimit(35);
    shooterTop.burnFlash();

    // Config Bottom Shooter
    shooterTop.setCANTimeout(250);
    shooterBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus3, Constants.periodicFrameDelay);
    shooterBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus4, Constants.periodicFrameDelay);
    shooterBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus5, Constants.periodicFrameDelay);
    shooterBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus6, Constants.periodicFrameDelay);
    pidControllerB = shooterBottom.getPIDController();
    pidControllerB.setFF(0.001849999971454963);
    pidControllerB.setP(0);
    pidControllerB.setI(0);
    pidControllerB.setD(0);
    shooterBottom.setSmartCurrentLimit(35);
    shooterBottom.burnFlash();
  }

  @Override
  public void updateInputs(ShooterIOInputs inputs) {
    inputs.speedLeft = shooterTop.get();
    inputs.speedRight = shooterBottom.get();
    Logger.recordOutput("Shooter/SpeedLeft", shooterTop.getEncoder().getVelocity());
    Logger.recordOutput("Shooter/SpeedRight", shooterBottom.getEncoder().getVelocity());
    Logger.recordOutput("Shooter/PivotPosition", pivotEncoder.getPosition());
    pivotController.setP(pivotPidP.get());
    pivotController.setI(pivotPidI.get());
  }

  @Override
  public void setSpeed(double speedLeft, double speedRight) {
    shooterTop.set(speedLeft);
    shooterBottom.set(speedRight);
  }

  @Override
  public void stop() {
    shooterTop.stopMotor();
    shooterBottom.stopMotor();
  }

  @Override
  public void setPIDFSpeed(double left, double right) {
    pidControllerT.setReference(left, ControlType.kVelocity, 0);
    pidControllerB.setReference(right, ControlType.kVelocity, 0);
  }

  @Override
  public void setVoltageTop(double voltage) {
    shooterTop.setVoltage(voltage);
  }

  @Override
  public void setVoltageBottom(double voltage) {
    shooterBottom.setVoltage(voltage);
  }

  public void goToPosition(double position) {
    pivotActuator.set(pivotController.calculate(pivotEncoder.getPosition(), position));
    Logger.recordOutput(
        "pivotPID", pivotController.calculate(pivotEncoder.getPosition(), position));
  }

  @Override
  public void stopPivot() {
    pivotActuator.stopMotor();
  }

  @Override
  public void setPivotPosition(double position) {
    goToPosition(position);
  }
}
