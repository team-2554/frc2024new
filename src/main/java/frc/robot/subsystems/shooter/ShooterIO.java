package frc.robot.subsystems.shooter;

import org.littletonrobotics.junction.AutoLog;

public interface ShooterIO {
  @AutoLog
  public static class ShooterIOInputs {
    public double speedLeft = 0.0;
    public double speedRight = 0.0;
    public double pivotPosition = 0.0;
  }

  public default void updateInputs(ShooterIOInputs inputs) {}

  public default void setSpeed(double speedLeft, double speedRight) {}

  public default void setPIDFSpeed(double speedleft, double speedRight) {}

  public default void stop() {}

  public default void setVoltageTop(double voltage) {}

  public default void setVoltageBottom(double voltage) {}

  public default void setPivotPosition(double position) {}

  public default void stopPivot() {}
}
