package frc.robot.subsystems.shooter;

import static edu.wpi.first.units.Units.Volts;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine;
import org.littletonrobotics.junction.Logger;
import org.littletonrobotics.junction.networktables.LoggedDashboardNumber;

public class Shooter extends SubsystemBase {

  private final ShooterIO mShooter;
  private final ShooterIOInputsAutoLogged inputs = new ShooterIOInputsAutoLogged();
  private final SysIdRoutine shooterSysIdTop;
  private final SysIdRoutine shooterSysIdBottom;
  private final LoggedDashboardNumber pivotAngle;

  public Shooter(ShooterIO shooter) {
    mShooter = shooter;
    shooterSysIdTop =
        new SysIdRoutine(
            new SysIdRoutine.Config(
                null,
                null,
                null,
                (state) -> Logger.recordOutput("Shooter/SysIdStateTop", state.toString())),
            new SysIdRoutine.Mechanism(
                (voltage) -> {
                  setVoltageTop(voltage.in(Volts));
                },
                null,
                this));
    shooterSysIdBottom =
        new SysIdRoutine(
            new SysIdRoutine.Config(
                null,
                null,
                null,
                (state) -> Logger.recordOutput("Shooter/SysIdStateBottom", state.toString())),
            new SysIdRoutine.Mechanism(
                (voltage) -> {
                  setVoltageBottom(voltage.in(Volts));
                },
                null,
                this));
    pivotAngle = new LoggedDashboardNumber("shooterAngle", 0.095);
  }

  public void setVoltageTop(double voltage) {
    mShooter.setVoltageTop(voltage);
  }

  public void setVoltageBottom(double voltage) {
    mShooter.setVoltageBottom(voltage);
  }

  public Command sysIdQuasistaticTop(SysIdRoutine.Direction direction) {
    return shooterSysIdTop.quasistatic(direction);
  }

  public Command sysIdQuasistaticBottom(SysIdRoutine.Direction direction) {
    return shooterSysIdBottom.quasistatic(direction);
  }

  public Command sysIdDynamicTop(SysIdRoutine.Direction direction) {
    return shooterSysIdTop.dynamic(direction);
  }

  public Command sysIdDynamicBottom(SysIdRoutine.Direction direction) {
    return shooterSysIdBottom.dynamic(direction);
  }

  @Override
  public void periodic() {
    mShooter.updateInputs(inputs);
    Logger.processInputs("Shooter/Shooter", inputs);
  }

  public double getSpeedLeft() {
    return inputs.speedLeft;
  }

  public double getSpeedRight() {
    return inputs.speedRight;
  }

  public void stop() {
    mShooter.stop();
  }

  public void setSpeed(double leftSpeed, double rightSpeed) {
    mShooter.setSpeed(leftSpeed, -rightSpeed);
  }

  public void setSpeed(double Speed) {
    mShooter.setSpeed(Speed, -Speed);
  }

  public void setPivotPosition(double position) {
    mShooter.setPivotPosition(position);
  }

  public Command pivotUnderStage() {
    return this.run(() -> setPivotPosition(0.04)).finallyDo(this::stopPivot);
  }

  public Command pivotShoot() {
    return this.run(() -> setPivotPosition(pivotAngle.get())).finallyDo(this::stopPivot);
  }

  public void stopPivot() {
    mShooter.stopPivot();
  }

  public void setPIDFSpeed(double leftSpeed, double rightSpeed) {
    mShooter.setPIDFSpeed(leftSpeed, -rightSpeed);
  }
}
