package frc.robot.subsystems.intake;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkLowLevel.PeriodicFrame;
import com.revrobotics.CANSparkMax;
import frc.robot.Constants;

public class IntakeIOTalon implements IntakeIO {

  private final VictorSPX intakeBack, intakeFront;
  private final CANSparkMax raiserBottom, raiserTop;

  public IntakeIOTalon() {
    intakeFront = new VictorSPX(Constants.intakeFront);
    intakeBack = new VictorSPX(Constants.intakeBack);
    raiserBottom = new CANSparkMax(Constants.raiserBottom, MotorType.kBrushless);
    raiserTop = new CANSparkMax(Constants.raiserTop, MotorType.kBrushless);

    raiserBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus2, Constants.periodicFrameDelay);
    raiserBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus3, Constants.periodicFrameDelay);
    raiserBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus4, Constants.periodicFrameDelay);
    raiserBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus5, Constants.periodicFrameDelay);
    raiserBottom.setPeriodicFramePeriod(PeriodicFrame.kStatus6, Constants.periodicFrameDelay);

    raiserTop.setPeriodicFramePeriod(PeriodicFrame.kStatus2, Constants.periodicFrameDelay);
    raiserTop.setPeriodicFramePeriod(PeriodicFrame.kStatus3, Constants.periodicFrameDelay);
    raiserTop.setPeriodicFramePeriod(PeriodicFrame.kStatus4, Constants.periodicFrameDelay);
    raiserTop.setPeriodicFramePeriod(PeriodicFrame.kStatus5, Constants.periodicFrameDelay);
    raiserTop.setPeriodicFramePeriod(PeriodicFrame.kStatus6, Constants.periodicFrameDelay);

    intakeFront.setStatusFramePeriod(
        StatusFrame.Status_12_Feedback1,
        Constants.periodicFrameDelay,
        Constants.periodicFrameDelay + 100);
    intakeBack.setStatusFramePeriod(
        StatusFrame.Status_12_Feedback1,
        Constants.periodicFrameDelay,
        Constants.periodicFrameDelay + 100);
  }

  @Override
  public void updateInputs(IntakeIOInputs inputs) {
    inputs.speedBackInput = intakeBack.getMotorOutputPercent();
    inputs.speedFrontIntake = intakeFront.getMotorOutputPercent();
    inputs.speedRaisingBottom = raiserBottom.get();
    inputs.speedRaisingTop = raiserBottom.get();
  }

  // @Override
  // public void setFront(double speed) {
  //   intakeFront.set(ControlMode.PercentOutput, speed);
  // }

  @Override
  public void setBack(double speed) {
    intakeBack.set(ControlMode.PercentOutput, speed);
  }

  @Override
  public void setRaiser(double speedBottom, double speedTop) {
    raiserBottom.set(speedBottom);
    raiserTop.set(speedTop);
  }

  @Override
  public void stopAll() {
    intakeBack.set(ControlMode.PercentOutput, 0);
    intakeFront.set(ControlMode.PercentOutput, 0);
    raiserBottom.stopMotor();
    raiserTop.stopMotor();
  }
}
