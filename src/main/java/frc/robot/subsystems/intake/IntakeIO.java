package frc.robot.subsystems.intake;

import org.littletonrobotics.junction.AutoLog;

public interface IntakeIO {

  @AutoLog
  public static class IntakeIOInputs {
    public double speedFrontIntake = 0.0;
    public double speedBackInput = 0.0;
    public double speedRaisingBottom = 0.0;
    public double speedRaisingTop = 0.0;
  }

  public default void updateInputs(IntakeIOInputs inputs) {}

  public default void setFront(double speedFront) {}

  public default void setBack(double speedBack) {}

  public default void setRaiser(double speedRaising) {}

  public default void setRaiser(double speedRaisingBottom, double speedRaisingTop) {}

  public default void stopAll() {}
}
