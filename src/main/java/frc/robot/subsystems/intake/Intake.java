package frc.robot.subsystems.intake;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.littletonrobotics.junction.AutoLogOutput;
import org.littletonrobotics.junction.Logger;

public class Intake extends SubsystemBase {

  private final IntakeIO mIntake;
  private final IntakeIOInputsAutoLogged inputs = new IntakeIOInputsAutoLogged();
  private final DigitalInput noteDetector;

  public Intake(IntakeIO intake) {
    mIntake = intake;
    noteDetector = new DigitalInput(5);
    noteState = noteDetector.get();
  }

  public void periodic() {
    mIntake.updateInputs(inputs);
    Logger.processInputs("Intake/Raiser", inputs);
    Logger.recordOutput("Intake/NoteDetector", isNote());
  }

  /*public double getSpeedFront() {
    return inputs.speedFrontIntake;
  }t */

  public double getSpeedBack() {
    return inputs.speedBackInput;
  }

  public void stopIntakes() {
    mIntake.stopAll();
  }

  /* public void setFront(double speed) {
    mIntake.setFront(-0.5);
    mIntake.setRaiser(speed, speed);
  } */

  public void setBack(double speed) {
    mIntake.setBack(0.5);
    mIntake.setRaiser(speed, -speed);
  }

  public void setRaiser(double speed) {
    mIntake.setRaiser(-speed, speed);
  }

  public void setRaiser(double bottom, double top) {
    mIntake.setRaiser(bottom, -top);
  }

  @AutoLogOutput()
  public boolean isNote() {
    return !noteDetector.get();
  }

  boolean noteState = true;

  public Command endOnNoteEnter() {
    return Commands.waitUntil(
        () -> {
          return noteRisingEdge();
        });
  }

  private boolean noteRisingEdge() {
    boolean noteDelta = !noteState && this.isNote();
    noteState = this.isNote();
    return noteDelta;
  }

  public Command endOnNoteLeave() {
    return Commands.waitUntil(
        () -> {
          boolean noteDelta = noteState && !this.isNote();
          noteState = this.isNote();
          return noteDelta;
        });
  }

  public Object setFront(double d) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'setFront'");
  }
}
