package frc.robot.subsystems.intake;

import edu.wpi.first.wpilibj.simulation.PWMSim;

public class IntakeIOSim implements IntakeIO {

  private final PWMSim intakeFront = new PWMSim(0);
  private final PWMSim intakeBack = new PWMSim(0);
  private final PWMSim raising = new PWMSim(0);

  @Override
  public void updateInputs(IntakeIOInputs inputs) {
    inputs.speedBackInput = intakeBack.getSpeed();
    inputs.speedFrontIntake = intakeFront.getSpeed();
    inputs.speedRaisingBottom = raising.getSpeed();
  }

  @Override
  public void setFront(double speed) {
    intakeFront.setSpeed(speed);
  }

  @Override
  public void setBack(double speed) {
    intakeBack.setSpeed(speed);
  }

  @Override
  public void setRaiser(double speed) {
    raising.setSpeed(speed);
  }

  @Override
  public void stopAll() {
    intakeFront.setSpeed(0);
    intakeBack.setSpeed(0);
    raising.setSpeed(0);
  }
}
