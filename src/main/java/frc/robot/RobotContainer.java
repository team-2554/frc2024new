// Copyright 2021-2024 FRC 6328
// http://github.com/Mechanical-Advantage
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// version 3 as published by the Free Software Foundation or
// available in the root directory of this project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// FRC 2024 by Aditya Saini, Amurth Nadimpally, Naishadh Patel,
// Tanish Mittal, Sarthak Kumar, Abhinav Senthilkumar, Ved Dwivedi, Dhairya Sarvaiya

package frc.robot;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.path.PathPlannerPath;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ProxyCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.CommandGenericHID;
import edu.wpi.first.wpilibj2.command.button.CommandPS4Controller;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine.Direction;
import frc.robot.Constants.Mode;
import frc.robot.commands.AlignToSpeaker;
import frc.robot.commands.AutoUtilCommand;
import frc.robot.commands.DriveCommands;
import frc.robot.subsystems.climber.Climber;
import frc.robot.subsystems.climber.ClimberIO;
import frc.robot.subsystems.climber.ClimberIOSim;
import frc.robot.subsystems.climber.ClimberIOSparkMax;
import frc.robot.subsystems.drive.Drive;
import frc.robot.subsystems.drive.GyroIO;
import frc.robot.subsystems.drive.GyroIONavX;
import frc.robot.subsystems.drive.ModuleIO;
import frc.robot.subsystems.drive.ModuleIOSim;
import frc.robot.subsystems.drive.ModuleIOSparkMax;
import frc.robot.subsystems.intake.Intake;
import frc.robot.subsystems.intake.IntakeIO;
import frc.robot.subsystems.intake.IntakeIOSim;
import frc.robot.subsystems.intake.IntakeIOTalon;
import frc.robot.subsystems.led.Led;
import frc.robot.subsystems.shooter.Shooter;
import frc.robot.subsystems.shooter.ShooterIO;
import frc.robot.subsystems.shooter.ShooterIOSim;
import frc.robot.subsystems.shooter.ShooterIOSparkMax;
import frc.robot.subsystems.vision.Limelight;
import java.util.function.DoubleSupplier;
import org.littletonrobotics.junction.networktables.LoggedDashboardChooser;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // Subsystems
  private final Drive drive;
  private final Shooter shooter;
  private final Intake intake;
  private final Climber climber;
  private final Limelight vision;
  private final Led mled;

  // Controller
  private final CommandPS4Controller controller = new CommandPS4Controller(0);
  private final CommandGenericHID secondControl = new CommandGenericHID(1);
  private final DoubleSupplier xSupplier = () -> -controller.getLeftY();
  private final DoubleSupplier ySupplier = () -> -controller.getLeftX();

  // Dashboard inputs

  private final LoggedDashboardChooser<Command> autoChooser;

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    switch (Constants.currentMode) {
      case REAL:
        // Real robot, instantiate hardware IO implementations
        drive =
            new Drive(
                new GyroIONavX(),
                new ModuleIOSparkMax(0),
                new ModuleIOSparkMax(1),
                new ModuleIOSparkMax(2),
                new ModuleIOSparkMax(3));
        shooter = new Shooter(new ShooterIOSparkMax());
        intake = new Intake(new IntakeIOTalon());
        climber = new Climber(new ClimberIOSparkMax());
        vision = new Limelight();
        mled = new Led();
        break;

      case SIM:
        // Sim robot, instantiate physics sim IO implementations
        drive =
            new Drive(
                new GyroIO() {},
                new ModuleIOSim(),
                new ModuleIOSim(),
                new ModuleIOSim(),
                new ModuleIOSim());
        shooter = new Shooter(new ShooterIOSim());
        intake = new Intake(new IntakeIOSim());
        climber = new Climber(new ClimberIOSim());
        vision = new Limelight();
        mled = new Led();
        break;

      default:
        // Replayed robot, disable IO implementations
        drive =
            new Drive(
                new GyroIO() {},
                new ModuleIO() {},
                new ModuleIO() {},
                new ModuleIO() {},
                new ModuleIO() {});
        shooter = new Shooter(new ShooterIO() {});
        intake = new Intake(new IntakeIO() {});
        climber = new Climber(new ClimberIO() {});
        vision = new Limelight();
        mled = new Led();
        break;
    }

    configureNamedCommands();
    autoChooser = new LoggedDashboardChooser<>("Auto Choices", AutoBuilder.buildAutoChooser());
    autoChooser.addOption(
        "Choreo Amp close",
        AutoBuilder.followPath(PathPlannerPath.fromChoreoTrajectory("AMP auto close")));
    autoChooser.addOption(
        "Choreo Source",
        AutoBuilder.followPath(PathPlannerPath.fromChoreoTrajectory("SOURCE auto far")));

    // Set up SysId routines
    autoChooser.addOption(
        "Drive SysId (Quasistatic Forward)", drive.sysIdQuasistatic(Direction.kForward));
    autoChooser.addOption(
        "Drive SysId (Quasistatic Reverse)", drive.sysIdQuasistatic(Direction.kReverse));
    autoChooser.addOption("Drive SysId (Dynamic Forward)", drive.sysIdDynamic(Direction.kForward));
    autoChooser.addOption("Drive SysId (Dynamic Reverse)", drive.sysIdDynamic(Direction.kReverse));
    autoChooser.addOption(
        "Shooter SysIdTop (Quasistatic)", shooter.sysIdQuasistaticTop(Direction.kForward));
    autoChooser.addOption(
        "Shooter SysIdTop (Dynamic)", shooter.sysIdDynamicTop(Direction.kForward));
    autoChooser.addOption(
        "Shooter SysIdBottom (Quasistatic)", shooter.sysIdQuasistaticBottom(Direction.kReverse));
    autoChooser.addOption(
        "Shooter SysIdBottom (Dynamic)", shooter.sysIdDynamicBottom(Direction.kReverse));

    // Configure the button bindings
    configureButtonBindings();
    configureSecondController();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {

    // default commands

    mled.setDefaultCommand(
        new InstantCommand(() -> mled.distance(drive.poseRadius(), intake.isNote()), mled));

    shooter.setDefaultCommand(
        new RunCommand(
                () -> {
                  if (intake.isNote()) {
                    shooter.setSpeed(0.25, 0.25);
                  } else {
                    shooter.stop();
                  }
                })
            .alongWith(shooter.pivotShoot()));

    drive.setDefaultCommand(
        DriveCommands.joystickDrive(drive, xSupplier, ySupplier, () -> -controller.getRightX()));

    controller.cross().onTrue(Commands.runOnce(drive::stopWithX, drive));
    controller
        .L1()
        .whileTrue(
            Commands.runEnd(
                () -> {
                  shooter.setSpeed(0.30, 0.30);
                },
                () -> {
                  shooter.stop();
                },
                shooter));
    controller
        .R3()
        .whileTrue(
            Commands.runEnd(
                () -> {
                  intake.setRaiser(0, -1);
                },
                () -> {
                  intake.stopIntakes();
                },
                intake));
    controller
        .R1()
        .whileTrue(
            Commands.runEnd(
                () -> {
                  intake.setBack(-0.25);
                },
                () -> {
                  intake.setBack(0);
                },
                intake));
    controller
        .circle()
        .onTrue(
            Commands.runOnce(
                    () ->
                        drive.setPose(
                            new Pose2d(drive.getPose().getTranslation(), new Rotation2d())),
                    drive)
                .ignoringDisable(true));

    controller
        .povUp()
        .whileTrue(
            Commands.startEnd(
                () -> {
                  climber.setSpeed(0.3);
                },
                () -> {
                  climber.stop();
                },
                climber));

    controller
        .povDown()
        .whileTrue(
            Commands.startEnd(
                () -> {
                  climber.setSpeed(-1);
                },
                () -> {
                  climber.stop();
                },
                climber));
    controller
        .povLeft()
        .whileTrue(
            Commands.startEnd(
                () -> {
                  climber.setWithPID(-90);
                },
                () -> {
                  climber.stop();
                },
                climber));
  }

  private void configureSecondController() {
    secondControl
        .button(15) // run SHOOT POSITION
        .whileTrue(shooter.pivotShoot());

    secondControl
        .button(21) // run SHOOT UNDER STAGE POSITION
        .whileTrue(shooter.pivotUnderStage());

    // secondControl.button().whileTrue();

    /* .button(5) // run FRONT intake
    .whileTrue(
        Commands.startEnd(
                () -> {
                  intake.setFront(0.5);
                },
                () -> {
                  intake.stopIntakes();
                },
                intake)
            .raceWith(intake.endOnNoteEnter())); */
    secondControl
        .button(4) // run BACK intake
        .whileTrue(
            Commands.startEnd(
                    () -> {
                      intake.setBack(-0.5);
                    },
                    () -> {
                      intake.stopIntakes();
                    },
                    intake)
                .raceWith(intake.endOnNoteEnter()));
    secondControl
        .button(2) // run FRONT raiser
        .whileTrue(
            Commands.startEnd(
                () -> {
                  intake.setRaiser(0, 0.75);
                },
                () -> {
                  intake.setRaiser(0, 0);
                },
                intake));
    secondControl
        .button(1) // run BACK raiser
        .whileTrue(
            Commands.startEnd(
                () -> {
                  intake.setRaiser(0, -0.75);
                },
                () -> {
                  intake.setRaiser(0, 0);
                },
                intake));

    secondControl // climber up/down
        .button(3)
        .whileTrue(
            Commands.startEnd(
                () -> {
                  climber.setSpeed(-0.5);
                },
                () -> {
                  climber.stop();
                },
                climber));

    secondControl
        .button(22)
        .whileTrue(
            Commands.startEnd(
                    () -> {
                      shooter.setSpeed(0.75, 0.75);
                    },
                    () -> {
                      shooter.stop();
                    },
                    shooter)
                .alongWith(Commands.run(() -> mled.shoot(), mled)));

    secondControl // shoot for amp
        .button(18)
        .whileTrue(
            Commands.startEnd(
                () -> {
                  shooter.setSpeed(1, -1);
                },
                () -> {
                  shooter.stop();
                },
                shooter));

    secondControl // turn to speaker from anywhere
        .button(13)
        .whileTrue(new ProxyCommand(() -> drive.turnToSpeaker(xSupplier, ySupplier)));

    secondControl // go to speaker from anywhere
        .button(12)
        .whileTrue(new ProxyCommand(() -> drive.goToSpeaker()));

    secondControl // go to amp from anywhere
        .button(14)
        .whileTrue(new ProxyCommand(() -> drive.goToAmp()));

    secondControl // go to source from anywhere
        .button(16)
        .whileTrue(new ProxyCommand(() -> drive.goToSource()));

    secondControl
        .button(7)
        .whileTrue((new DriveCommands.alignToNote(vision, 0, drive, xSupplier, ySupplier)));
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public void configureNamedCommands() {
    NamedCommands.registerCommand("IntakeFront", AutoUtilCommand.IntakeFront(intake));
    NamedCommands.registerCommand("IntakeBack", AutoUtilCommand.IntakeBack(intake));
    NamedCommands.registerCommand(
        "IntakeStop",
        new InstantCommand(
            () -> {
              intake.stopIntakes();
            }));
    NamedCommands.registerCommand("AlignToSpeaker", new AlignToSpeaker(drive));
    NamedCommands.registerCommand("Shoot", AutoUtilCommand.Shoot(shooter, intake));
    NamedCommands.registerCommand("ShootToSpeaker", AutoUtilCommand.Shoot(shooter, intake));
    NamedCommands.registerCommand("autoShoot", AutoUtilCommand.shootNoteAuto(intake));
    NamedCommands.registerCommand(
        "TurnToSpeaker",
        new ProxyCommand(() -> drive.turnToSpeaker(xSupplier, ySupplier).repeatedly()));
  }

  public Command getAutonomousCommand() {
    if (Constants.currentMode == Mode.SIM) {
      return autoChooser.get();
    } else {
      return autoChooser.get().alongWith(AutoUtilCommand.autoIdleShooter(shooter, intake));
    }
  }

  public void testPeriodic() {}
}
