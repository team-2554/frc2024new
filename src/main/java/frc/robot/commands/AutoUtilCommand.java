package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.intake.Intake;
import frc.robot.subsystems.shooter.Shooter;

public class AutoUtilCommand {
  private AutoUtilCommand() {}

  public static Command Shoot(Shooter shooter, Intake intake) {
    return new SequentialCommandGroup(
        new InstantCommand(
                () -> {
                  shooter.setPIDFSpeed(5000, 5000);
                },
                shooter)
            .andThen(new WaitCommand(2))
            .andThen(
                new InstantCommand(
                    () -> {
                      intake.setRaiser(0, -0.5);
                    },
                    intake))
            .andThen(new WaitCommand(2))
            .andThen(shooter::stop, shooter)
            .andThen(() -> intake.setRaiser(0, 0), intake));
  }

  public static Command autoIdleShooter(Shooter shooter, Intake intake) {
    return shooter.run(() -> shooter.setSpeed(intake.isNote() ? 0.75 : 0));
  }

  public static Command shootNoteAuto(Intake intake) {
    return intake
        .runEnd(() -> intake.setRaiser(0, -1), () -> intake.setRaiser(0))
        .raceWith(intake.endOnNoteLeave());
  }

  public static Command IntakeFront(Intake intake) {
    return intake.run(() -> intake.setFront(0.5)).raceWith(intake.endOnNoteEnter());
  }

  public static Command IntakeBack(Intake intake) {
    return intake.run(() -> intake.setBack(0.5)).raceWith(intake.endOnNoteEnter());
  }
}
