package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.drive.*;

public class AlignToSpeaker extends Command {
  private final Drive drive;

  public AlignToSpeaker(Drive drivetrain) {
    this.drive = drivetrain;
    addRequirements(drivetrain);
  }

  @Override
  public void execute() {
    this.drive.turnToSpeaker();
  }

  public boolean isFinished() {
    return false;
  }

  public void end(boolean interrupted) {
    this.drive.stop();
  }
}
