// Copyright 2021-2024 FRC 6328
// http://github.com/Mechanical-Advantage
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// version 3 as published by the Free Software Foundation or
// available in the root directory of this project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Transform2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import frc.robot.subsystems.drive.Drive;
import frc.robot.subsystems.vision.Limelight;
import java.util.function.DoubleSupplier;
import org.littletonrobotics.junction.Logger;

public class DriveCommands {
  private static final double DEADBAND = 0.1;
  private static PIDController thetaController = new PIDController(0.57, 0, 0);

  private DriveCommands() {}

  private static ChassisSpeeds getFieldRelativeChassisSpeeds(
      Drive drive,
      DoubleSupplier xSupplier,
      DoubleSupplier ySupplier,
      DoubleSupplier omegaSupplier) {
    // Apply deadband
    double linearMagnitude =
        MathUtil.applyDeadband(
            Math.hypot(xSupplier.getAsDouble(), ySupplier.getAsDouble()), DEADBAND);
    Rotation2d linearDirection = new Rotation2d(xSupplier.getAsDouble(), ySupplier.getAsDouble());
    double omega = MathUtil.applyDeadband(omegaSupplier.getAsDouble(), DEADBAND);

    // Square values
    linearMagnitude = linearMagnitude * linearMagnitude;
    omega = Math.copySign(omega * omega, omega);

    // Calcaulate new linear velocity
    Translation2d linearVelocity =
        new Pose2d(new Translation2d(), linearDirection)
            .transformBy(new Transform2d(linearMagnitude, 0.0, new Rotation2d()))
            .getTranslation();

    // Convert to field relative speeds & send command
    boolean isFlipped =
        DriverStation.getAlliance().isPresent()
            && DriverStation.getAlliance().get() == Alliance.Red;
    return ChassisSpeeds.fromFieldRelativeSpeeds(
        linearVelocity.getX() * drive.getMaxLinearSpeedMetersPerSec(),
        linearVelocity.getY() * drive.getMaxLinearSpeedMetersPerSec(),
        omega * drive.getMaxAngularSpeedRadPerSec(),
        isFlipped ? drive.getRotation().plus(new Rotation2d(Math.PI)) : drive.getRotation());
  }

  /**
   * Field relative drive command using two joysticks (controlling linear and angular velocities).
   */
  public static Command joystickDrive(
      Drive drive,
      DoubleSupplier xSupplier,
      DoubleSupplier ySupplier,
      DoubleSupplier omegaSupplier) {
    return Commands.run(
        () -> {
          drive.runVelocity(
              getFieldRelativeChassisSpeeds(drive, xSupplier, ySupplier, omegaSupplier));
        },
        drive);
  }

  public static Command turnToAnglePID(DoubleSupplier remaining, Drive drive) {
    return new PIDCommand(
        thetaController,
        remaining,
        0,
        (double outputs) -> {
          drive.runVelocity(
              new ChassisSpeeds(
                  0 * drive.getMaxLinearSpeedMetersPerSec(),
                  0 * drive.getMaxLinearSpeedMetersPerSec(),
                  outputs * drive.getMaxAngularSpeedRadPerSec()));
        },
        drive);
  }

  public static Command turnToAnglePID(
      DoubleSupplier remaining, Drive drive, DoubleSupplier xSupplier, DoubleSupplier ySupplier) {

    return new PIDCommand(
        thetaController,
        remaining,
        0,
        (double outputs) -> {
          ChassisSpeeds joystickInput =
              getFieldRelativeChassisSpeeds(drive, xSupplier, ySupplier, () -> 0);
          Logger.recordOutput("pidTurnOut", outputs);
          drive.runVelocity(
              new ChassisSpeeds(
                  joystickInput.vxMetersPerSecond,
                  joystickInput.vyMetersPerSecond,
                  outputs * drive.getMaxAngularSpeedRadPerSec()));
        },
        drive);
  }

  public static class alignToNote extends Command {
    int objectIndex;
    Limelight vision;
    Drive mdrive;
    Command pidCommandReference;
    DoubleSupplier xDoubleSupplier;
    DoubleSupplier yDoubleSupplier;
    double noteTheta = 0;
    double remaining = 0;

    public alignToNote(
        Limelight vision,
        int objectIndex,
        Drive mdrive,
        DoubleSupplier xDoubleSupplier,
        DoubleSupplier yDoubleSupplier) {
      this.vision = vision;
      this.objectIndex = objectIndex;
      this.mdrive = mdrive;
      this.xDoubleSupplier = xDoubleSupplier;
      this.yDoubleSupplier = yDoubleSupplier;
    }

    @Override
    public void initialize() {
      vision.setPipeline(objectIndex);
      pidCommandReference =
          DriveCommands.turnToAnglePID(() -> remaining, mdrive, xDoubleSupplier, yDoubleSupplier)
              .repeatedly();
      pidCommandReference.schedule();
      Logger.recordOutput("Auto/started", "init");
      remaining = Units.degreesToRadians(mdrive.getRotation().getDegrees() - noteTheta);
    }

    @Override
    public void execute() {
      if (vision.hasObjects() && Math.abs(vision.getXOffset() - noteTheta) > 5) {
        noteTheta = mdrive.getRotation().getDegrees() - vision.getXOffset();
      }
      remaining = Units.degreesToRadians(mdrive.getRotation().getDegrees() - noteTheta);

      Logger.recordOutput("Auto/noteTheta", noteTheta);
      Logger.recordOutput("Auto/remaining", remaining);
    }

    @Override
    public void end(boolean interupted) {
      pidCommandReference.cancel();
    }

    @Override
    public boolean isFinished() {
      return false;
    }
  }
}
