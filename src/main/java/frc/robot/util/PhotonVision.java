package frc.robot.util;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DriverStation;
import java.io.IOException;
import java.util.Optional;
import org.photonvision.*;
import org.photonvision.PhotonPoseEstimator.PoseStrategy;

public class PhotonVision {

  PhotonCamera camera;

  PhotonPoseEstimator estimator;

  public PhotonVision() {
    camera = new PhotonCamera("primary");
    // camera2 = new PhotonCamera("Arducam_OV9282_USB_Camera");
    try {
      AprilTagFieldLayout fieldLayout =
          AprilTagFieldLayout.loadFromResource(AprilTagFields.k2024Crescendo.m_resourceFile);
      estimator =
          new PhotonPoseEstimator(
              fieldLayout,
              PoseStrategy.MULTI_TAG_PNP_ON_COPROCESSOR,
              camera,
              new Transform3d(
                  Units.inchesToMeters(8.495),
                  Units.inchesToMeters(-3.003),
                  Units.inchesToMeters(7.848),
                  new Rotation3d(Units.degreesToRadians(-90), Units.degreesToRadians(25), 0)));

    } catch (IOException io) {
      DriverStation.reportError("Failed to load AprilTagLayout.", io.getStackTrace());
    }
  }

  public Optional<EstimatedRobotPose> getEstimatedGlobalPose(Pose2d prevEstimatedPose) {
    estimator.setReferencePose(prevEstimatedPose);
    return estimator.update();
  }
}
