// Copyright 2021-2024 FRC 6328
// http://github.com/Mechanical-Advantage
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// version 3 as published by the Free Software Foundation or
// available in the root directory of this project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
  public static final Mode currentMode = Mode.REAL;

  // CAN IDs
  public static final int shooterTop = 7;
  public static final int shooterBottom = 3;
  public static final int intakeFront = 43;
  public static final int intakeBack = 42;
  public static final int raiserBottom = 31;
  public static final int raiserTop = 30;
  public static final int flDrive = 21;
  public static final int flTurn = 22;
  public static final int frDrive = 20;
  public static final int frTurn = 19;
  public static final int blDrive = 15;
  public static final int blTurn = 16;
  public static final int brDrive = 18;
  public static final int brTurn = 17;
  // need to find can id
  public static final int climber = 62;
  public static final int pivotActuator = 40;

  // CAN Frame delays
  public static final int periodicFrameDelay = 10000;

  public static enum Mode {
    /** Running on a real robot. */
    REAL,

    /** Running a physics simulator. */
    SIM,

    /** Replaying from a log file. */
    REPLAY
  }
}
