package frc.robot;

/** Automatically generated file containing build version information. */
public final class BuildConstants {
  public static final String MAVEN_GROUP = "";
  public static final String MAVEN_NAME = "frc2024new";
  public static final String VERSION = "unspecified";
  public static final int GIT_REVISION = 136;
  public static final String GIT_SHA = "ad515bd3451c118e9b8b9a144602d6de12856f33";
  public static final String GIT_DATE = "2024-09-13 15:41:10 EDT";
  public static final String GIT_BRANCH = "main";
  public static final String BUILD_DATE = "2024-09-19 15:29:52 EDT";
  public static final long BUILD_UNIX_TIME = 1726774192239L;
  public static final int DIRTY = 1;

  private BuildConstants() {}
}
